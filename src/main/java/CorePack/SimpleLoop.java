package CorePack;

public class SimpleLoop {

  public static void main(String[] args) {

    printNumbers();

    calculateSumOfNumbers();

  }

//---------------------------------------- * * * ------------------------------------------

  public static void printNumbers() {

    for (int i=1; i<=10; i++) {
      System.out.println(i);
    }

  }

//---------------------------------------- * * * ------------------------------------------

  public static void calculateSumOfNumbers() {

    int i=1;
    int sum=0;

    while (i<=10) {
      sum+=i;
      i++;
    }

    System.out.println(sum);

  }

//---------------------------------------- * * * ------------------------------------------

}
