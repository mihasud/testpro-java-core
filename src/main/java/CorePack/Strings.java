package CorePack;

public class Strings {

  public static void main(String[] args) {

    replaceMerecedesWithBmw();

    countCharacters();

    compareStrings();

  }

//---------------------------------------- * * * ------------------------------------------

  public static void replaceMerecedesWithBmw() {

    String string1 = "Mercedes best german car";
    String string2 = string1.replace("Mercedes", "BMW");
    System.out.println(string2);

  }

//---------------------------------------- * * * ------------------------------------------

  public static void countCharacters() {

    String text = "I am a string";
    System.out.println(text.length());

  }

//---------------------------------------- * * * ------------------------------------------

  public static void compareStrings() {

    String a = "Cat";
    String b = "CAT";
    System.out.println(a.equalsIgnoreCase(b));

  }

//---------------------------------------- * * * ------------------------------------------

}
