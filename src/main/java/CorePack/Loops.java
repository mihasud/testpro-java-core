package CorePack;

public class Loops {

  public static void main(String[] args) {

    //calculateSum();

    //fibonacci();

    primeNumbers();
    primeNumbers2();

    //stringEvenOrNot();


  }

// Write a program to calculate the sum of following series where n is input by user: 1 + 1/2 + 1/3 + 1/4 + 1/5 + ... + 1/n

  public static void calculateSum() {

    int n = 10;
    double result = 0;
    for (int i = 1; i <= n; i++) {
      result += 1.0/i;
      System.out.println(result);
    }
  }

//---------------------------------------- * * * ------------------------------------------

// Write a program to print Fibonacci series of n terms where n is input by user: 0 1 1 2 3 5 8 13 21 ...

  public static void fibonacci() {

    int i = 0;
    int n = 8;
    int a = 0;
    int b = 1;

    for (i = 1; i < n; i++) {
      System.out.println(a);
      int c = a + b;
      a = b;
      b = c;
    }
    System.out.println("Index" + i + " = " + a);
  }

//---------------------------------------- * * * ------------------------------------------

// Write a program that prompts the user to input a positive integer. It should then output a message indicating where the number is a prime or not.

  public static void primeNumbers() {

    int number = 5;
    boolean flag = true;

    for(int i = 2; i < number; i++) {
      if(number % i == 0) {
        flag = false;
        break;
      }
      System.out.println(number % i);
    }
    if(flag && number > 1) {
      System.out.println("Number is prime");
    }
    else {
      System.out.println("Number is not prime");
    }
  }

//---------------------------------------- * * * ------------------------------------------

    public static void primeNumbers2() {

    int number = 7;

      for (int i = 2; i < number; i++) {

        if (number % i == 0) {
          System.out.println("This number is not prime");
          return;
        }
      }

      System.out.println("This number is prime");

    }

//---------------------------------------- * * * ------------------------------------------

// Create a program, which will accept a string and check if total count on characters within the string is even or not, should return String is even or String is not even

  public static void stringEvenOrNot() {

  }

//---------------------------------------- * * * ------------------------------------------
}

